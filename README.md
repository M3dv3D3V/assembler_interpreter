# Useful links

## 1. https://youtu.be/q6uF3a-SJUU
## 2. https://youtu.be/Ezt3vBok5_s
## 3. https://habr.com/ru/company/vk/blog/451894/
## 4. https://compilers.iecc.com/crenshaw/ (Уроки по созданию своего компилятора)
## 5. https://commandcenter.blogspot.com/2011/08/regular-expressions-in-lexing-and.html (Regular expressions in lexing and parsing)
## 6. https://docs.microsoft.com/ru-ru/archive/blogs/ericlippert/how-many-passes (Как работает компилятор C#)
## 7. https://cdsmith.wordpress.com/2011/01/09/an-old-article-i-wrote/ (Прочитать про системы типов)

# [Алгоритм работы компилятора](algorithm_compiler.md)
