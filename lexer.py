import sys


class TokenType:
    pass


class Mov(TokenType):
    pass


class Add(TokenType):
    pass


class Sub(TokenType):
    pass


class Mul(TokenType):
    pass


class Digit(TokenType):
    pass


class Register(TokenType):
    pass


class Syscall(TokenType):
    pass


class Xor(TokenType):
    pass


RESERVER_WORDS = {
    'mov': Mov,
    'add': Add,
    'sub': Sub,
    'mul': Mul,
    'syscall': Syscall,
    'xor': Xor
}


REGISTERS = [
    'rax', 'rcx', 'rdx', 'rbx', 'rsp',
    'rbp', 'rsi', 'rdi', 'r8', 'r9',
    'r10', 'r11', 'r12', 'r13', 'r14', 'r15'
]

class Token:
    def __init__(self, position: tuple, token_type: TokenType, namespace: str, value: str):
        self.__pos = position
        self.__token_type = token_type
        self.__namespace = namespace
        self.__value = value

    def get_position(self):
        return self.__pos

    def get_token_type(self):
        return self.__token_type

    def get_namespace(self):
        return self.__namespace

    def get_value(self):
        return self.__value      


class Lexer:
    """
    The first stage of the compiler/interpreter is the lexer.
    The output is a stream of tokens
    """
    def __init__(self, path_to_src: str) -> None:
        self.__path_to_src = path_to_src
        self.__tokens = []

    def __get_data_from_file(self) -> list:
        result = []
        if not self.__path_to_src:
            return result

        with open(self.__path_to_src) as file_obj:
            result = file_obj.readlines()

        return result

    def __searching_lexemes_in_row(self, row: str) -> list:
        lexemes_list = list()
        lexeme = ''
        control_chars = ('\n', '\t', '\r')
        for column, symbol in enumerate(row):
            if not symbol:
                continue

            if symbol == ';':
                return lexemes_list

            if lexeme == ' ' and symbol == ' ':
                lexeme = ''
                continue

            if lexeme and symbol == ' ' or symbol == ',' or symbol in control_chars:
                lexeme = lexeme.strip()
                lexemes_list.append((column, lexeme))
                lexeme = ''
                continue

            lexeme += symbol

        return lexemes_list

    def __get_tokens(self, lexemes: list, index_row: str) -> list:
        result = []
        for index_column, lexeme in lexemes:
            token = ''
            position_lexeme = (index_row+1, index_column+1)

            if lexeme.lower() in RESERVER_WORDS:
                token = Token(position_lexeme, RESERVER_WORDS.get(lexeme), 'main', 'reserved_word')

            if lexeme.lower().isdigit():
                token = Token(position_lexeme, Digit, 'main', lexeme)

            if lexeme.lower() in REGISTERS:
                token = Token(position_lexeme, Register, 'main', lexeme)

            if token:
                result.append(token)

        return result

    def lex(self) -> list:
        raw_data_src = self.__get_data_from_file()
        lexemes = list()
        for index_row, row in enumerate(raw_data_src):
            if not row:
                continue

            lexemes_in_current_row = self.__searching_lexemes_in_row(row)
            if lexemes_in_current_row == ['']:
                continue

            current_tokens = self.__get_tokens(lexemes_in_current_row, index_row)

            lexemes.extend(current_tokens)

        return lexemes



def main():
    path_to_src = sys.argv[1]
    lexer = Lexer(path_to_src)
    tokens = lexer.lex()

    for token in tokens:
        position = token.get_position()
        # token_type = token.get_token_type()
        # namespace = token.get_namespace()
        value = token.get_value()
        # print(position, token_type, namespace, value)
        print(position, value)


if __name__ == '__main__':
    main()
